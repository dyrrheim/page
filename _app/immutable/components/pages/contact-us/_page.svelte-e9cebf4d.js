import{S as X,i as Y,s as Z,k as a,a as g,q as k,N as ee,l as s,h as n,c as b,m,r as A,n as t,O as Q,J as e,b as V,A as U}from"../../../chunks/index-98817282.js";function te($){let i,f,I,r,p,j,C,y,S,M,c,E,N,O,l,R,W,B,H,P,_,o,u,J,D,q,G;return{c(){i=a("meta"),f=a("meta"),I=g(),r=a("header"),p=a("img"),C=g(),y=a("h1"),S=k("Sic Infit"),M=g(),c=a("section"),E=a("h3"),N=k("The sky is the limit"),O=g(),l=a("p"),R=k(`Connections are crucial. With our services, you can
            skip the search and bureaucracy, and be quickly 
            introduced to the right people. 
            If you already have your contacts, 
            but still require a reliable communication,
            our training and translation services are also at your disposal.
            `),W=a("br"),B=a("br"),H=k(`
            This could be the start of something big.`),P=g(),_=a("a"),o=a("button"),u=a("img"),D=g(),q=a("p"),G=k("Whatsapp"),this.h()},l(h){const v=ee('[data-svelte="svelte-1edehhq"]',document.head);i=s(v,"META",{name:!0,content:!0}),f=s(v,"META",{name:!0,content:!0}),v.forEach(n),I=b(h),r=s(h,"HEADER",{class:!0});var d=m(r);p=s(d,"IMG",{id:!0,src:!0,alt:!0}),C=b(d),y=s(d,"H1",{});var K=m(y);S=A(K,"Sic Infit"),K.forEach(n),M=b(d),c=s(d,"SECTION",{class:!0});var w=m(c);E=s(w,"H3",{});var z=m(E);N=A(z,"The sky is the limit"),z.forEach(n),O=b(w),l=s(w,"P",{class:!0});var T=m(l);R=A(T,`Connections are crucial. With our services, you can
            skip the search and bureaucracy, and be quickly 
            introduced to the right people. 
            If you already have your contacts, 
            but still require a reliable communication,
            our training and translation services are also at your disposal.
            `),W=s(T,"BR",{}),B=s(T,"BR",{}),H=A(T,`
            This could be the start of something big.`),T.forEach(n),w.forEach(n),P=b(d),_=s(d,"A",{href:!0,class:!0});var F=m(_);o=s(F,"BUTTON",{class:!0,alt:!0});var x=m(o);u=s(x,"IMG",{id:!0,src:!0,alt:!0,class:!0}),D=b(x),q=s(x,"P",{});var L=m(q);G=A(L,"Whatsapp"),L.forEach(n),x.forEach(n),F.forEach(n),d.forEach(n),this.h()},h(){document.title=`
    Contact Us
`,t(i,"name","description"),t(i,"content","Our services are just a few clicks away"),t(f,"name","robots"),t(f,"content","index"),t(p,"id","company-logo"),Q(p.src,j="images/logo-no-bg-DARK.svg")||t(p,"src",j),t(p,"alt","company logo"),t(l,"class","svelte-i38i6a"),t(c,"class","svelte-i38i6a"),t(u,"id","whatsapp-logo"),Q(u.src,J="images/whatsapp-873316_640.png")||t(u,"src",J),t(u,"alt",""),t(u,"class","svelte-i38i6a"),t(o,"class","onAccent social-media"),t(o,"alt","whatsapp button"),t(_,"href","https://wa.me/"+ae),t(_,"class","svelte-i38i6a"),t(r,"class","svelte-i38i6a")},m(h,v){e(document.head,i),e(document.head,f),V(h,I,v),V(h,r,v),e(r,p),e(r,C),e(r,y),e(y,S),e(r,M),e(r,c),e(c,E),e(E,N),e(c,O),e(c,l),e(l,R),e(l,W),e(l,B),e(l,H),e(r,P),e(r,_),e(_,o),e(o,u),e(o,D),e(o,q),e(q,G)},p:U,i:U,o:U,d(h){n(i),n(f),h&&n(I),h&&n(r)}}}const ae=447915607729;function se($){return[]}class ne extends X{constructor(i){super(),Y(this,i,se,te,Z,{})}}export{ne as default};
